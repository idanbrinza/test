@extends('layouts.app')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<h1>Customer List:</h1>
<a href = "{{route('customers.create')}}">Add a new Customer </a>
<ul>

@foreach($customers as $customer)
@if($customer->user->id==$userid)
<li style = "font-weight:bold">
@else
<li>
@endif
    @if($customer->status)
<span style="color:green">{{$customer->name}}, {{$customer->email}}, {{$customer->phone}}, Created by: {{$customer->user->name}}</span>
    @else
<span>{{$customer->name}}, {{$customer->email}}, {{$customer->phone}}, Created by: {{$customer->user->name}}</span>
@endif
    <a href = "{{route('customers.edit',$customer->id)}}">Edit</a>
    @can('manager')
    <a style="color:red" href="{{route('customers.delete' , $customer->id)}}"> Delete</a> 
    @if(!($customer->status))
    <a href = "{{route('customers.deal',$customer->id)}}">Deal Closed</a>
@endif
    @endcan
    @cannot('manager')
    Delete         
    @endcannot



</li>
@endforeach
</ul>
@endsection